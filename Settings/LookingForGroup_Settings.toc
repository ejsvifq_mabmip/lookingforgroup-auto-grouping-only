## Interface: 80205
## Author: cqwrteur
## Title: LookingForGroup Settings
## LoadOnDemand: 1
## Dependencies: LookingForGroup
## SavedVariables: LookingForGroup_OptionsDB
## X-LFG-MESSAGE: LFG_ChatCommand,LFG_ICON_LEFT_CLICK
## X-LFG-EM-HOSTER: LookingForGroup_Options

#@no-lib-strip@
Libs\AceGUI-3.0\AceGUI-3.0.xml
Libs\AceConfig-3.0\AceConfig-3.0.xml
Libs\AceDBOptions-3.0\AceDBOptions-3.0.xml
Libs\AceLocale-3.0\AceLocale-3.0.xml
#@end-no-lib-strip@

options_init.lua
locale\locale.xml
settings.lua
auto.lua
rf.lua
sf.lua
